#pragma once
#include<malloc.h>
#include<cstdlib>
#include<string.h>
#include <new>
#include <iostream>
using namespace std;

using u_char = unsigned char;
using ngx_uint_t = unsigned int;
//清理外部空间的函数
typedef void (*ngx_pool_cleanup_pt)(void* data);
struct ngx_pool_s;
struct ngx_pool_cleanup_s {
    ngx_pool_cleanup_pt      handler;
    void*                   data;
    ngx_pool_cleanup_s*     next;
};

//大块内存的头信息
struct ngx_pool_large_s {
    ngx_pool_large_s*   next;
    void*               alloc;
};

//内存池的头部信息
struct ngx_pool_data_s {
    u_char*           last;
    u_char*           end;
    ngx_pool_s*     next;
    ngx_uint_t      failed;
};

//收个内存池的头信息
struct ngx_pool_s {
    ngx_pool_data_s         d;
    size_t                  max;  //大块内存小块内存的界限
    ngx_pool_s*             current; //第一个能分配内存的内存块
    ngx_pool_large_s*       large;//串起大块内存头信息的头结点
    ngx_pool_cleanup_s*     cleanup;//串起释放外部内存的信息的头节点
};

#define ngx_align(d, a)     (((d) + (a - 1)) & ~(a - 1))
//小块内存分配考虑字节对齐
#define NGX_ALIGNMENT  sizeof(unsigned  long)
//把指针调整到临近倍数
#define ngx_align_ptr(p, a)   \
        (u_char *) (((uintptr_t) (p) + ((uintptr_t) a - 1)) & ~((uintptr_t) a - 1))

#define ngx_memzero(buf, n)       (void) memset(buf, 0, n)

const int ngx_pagesize = 4096;

const int   NGX_MAX_ALLOC_FROM_POOL = (ngx_pagesize - 1);//

const int NGX_DEFAULT_POOL_SIZE = (16 * 1024);

const int  NGX_POOL_ALIGNMENT = 16;//字节对齐最小单位
const int NGX_MIN_POOL_SIZE = ngx_align((sizeof(ngx_pool_s) + 2 * sizeof(ngx_pool_large_s)), NGX_POOL_ALIGNMENT);//最小内存池


class ngx_mem_pool {
public:
    ngx_mem_pool(size_t size) {
        if (nullptr == ngx_create_pool(size))
        {
            throw bad_alloc();
        }
    }
    ~ngx_mem_pool() {
        cout << 1 << endl;
        ngx_destory_pool();
    }

    //创建size大小的内存池，最大不超过一个页面
    void* ngx_create_pool(size_t size);
    //考虑内存字节对齐，从内存池申请size大小的内存
    void* ngx_palloc(size_t size);
    //不考虑字节对齐
    void* ngx_pnalloc(size_t size);
    //考虑字节对齐，并初始化为0
    void* ngx_pcalloc(size_t size);
    //释放大块内存
    void  ngx_pfree(void* p);
    //内存池重置
    void ngx_reset_pool();
    //内存池销毁
    void ngx_destory_pool();
    //添加回调清理操作函数
    ngx_pool_cleanup_s* ngx_pool_cleanup_add(size_t size);

private:
    ngx_pool_s* pool;

    void* ngx_palloc_small(size_t size, ngx_uint_t align);

    void* ngx_palloc_large( size_t size);
    
    void* ngx_palloc_block( size_t size);
};

